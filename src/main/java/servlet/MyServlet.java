package servlet;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.model.WebPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Scanned
public class MyServlet extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(MyServlet.class);

    @ComponentImport
    private final WebInterfaceManager webInterfaceManager;

    @Inject
    private MyServlet( final WebInterfaceManager webInterfaceManager)
    {
        this.webInterfaceManager = webInterfaceManager;
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/html");


        Map<String, Object> context = new HashMap<>();

        resp.getWriter().write("<html><body>");
        for(WebPanel webPanel: this.webInterfaceManager.getWebPanels("my-location"))
        {
            resp.getWriter().write("<hr/>");

            resp.getWriter().write( webPanel.getHtml(context));
            resp.getWriter().write("<hr/>");
        }

        resp.getWriter().write("</body></html>");
    }

}